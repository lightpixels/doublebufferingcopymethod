#-------------------------------------------------
#
# Project created by QtCreator 2016-09-17T16:36:47
#
#-------------------------------------------------

QT      -= gui

TARGET   = DoubleBufferingCopyMethod

TEMPLATE = lib

include( ../deploy.pri )
DESTDIR = $$PROJECT_ROOT_DIRECTORY/deploy

DEFINES += DOUBLEBUFFERINGCOPYMETHOD_LIBRARY

SOURCES += doublebufferingcopymethod.c

HEADERS += doublebufferingcopymethod_global.h \
           doublebuffering.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
