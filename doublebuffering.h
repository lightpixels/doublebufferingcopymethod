#ifndef DOUBLEBUFFERINGCOPYMETHOD_H
#define DOUBLEBUFFERINGCOPYMETHOD_H

#include "doublebufferingcopymethod_global.h"

#include <stdint.h>

DOUBLEBUFFERINGCOPYMETHODSHARED_EXPORT void doubleBufferingCopy(uint16_t* pixelsFrom,
                                                                uint16_t* pixelsTo,
                                                                size_t    nPixels,
                                                                uint16_t* pixelsFront,
                                                                uint16_t* pixelsBack);

#endif // DOUBLEBUFFERINGCOPYMETHOD_H
